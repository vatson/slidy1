exports.slider =

  holder: null
  slides: []
  currentSlide: 0

  init: (holder) ->
    @holder = holder
    @slides = $(holder).find 'img'
    $(holder).
      hammer()
      .bind 'dragstart', () =>
        return false
      .bind 'dragend', (e) =>
        if e.direction == 'left'
          ss.rpc "slider.showNext"
        else if e.direction == 'right'
          ss.rpc "slider.showPrev"
    @hideAll()
    @show(0)

  showNext: ->
    if @currentSlide < @slides.size() - 1
      @hide(@currentSlide)
      @show ++@currentSlide

  showPrevious: ->
    if @currentSlide > 0
      @hide(@currentSlide)
      @show --@currentSlide

  show: (index) ->
    @currentSlide = index;
    $(@slides[index]).show()

  hide: (index) ->
    $(@slides[index]).hide()

  hideAll: ->
    $(slide).hide() for slide in @slides

ss.event.on "showNext", =>
  exports.slider.showNext()
ss.event.on "showPrev", =>
  exports.slider.showPrevious()