exports.uploader =
  init: (uploadArea)->
    uploadArea.ondragover = ->
      @className += " hover" if "hover" not in @className.split " "
      null

    uploadArea.ondragleave = ->
      @className = @className.replace " hover", ""
      null

    uploadArea.onclick = (e)->
      file = document.getElementById('file')
      file.onclick = (e) ->
        e.stopImmediatePropagation() # fixes to fire the file dialog twice
      file.click()
      null

    $('#'+uploadArea.id).find('input[type=file]').change (e)->
      formData = new FormData
      formData.append('file', file) for file in this.files

      xhr = new XMLHttpRequest
      xhr.open 'POST', '/upload'
      xhr.onload = ->
        alert xhr.status
      xhr.send formData
      @value = null