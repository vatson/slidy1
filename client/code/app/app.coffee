# Client Code

user = ss.rpc 'user.getCurrent', (response) ->
  html = ss.tmpl['login'].render
    user: response
  $("#dashboard").html(html)

u = require('/uploader.coffee')
s = require('/slider.coffee')

jQuery ->
  u.uploader.init document.getElementById('uploadArea')
  s.slider.init "#slider"
