zip = require('adm-zip')

uncompressTypes = [
  'application/zip'
]

exports.supportsUncompress = (type) ->
  type in uncompressTypes

exports.uncompress = (type, archivePath, targetDir) ->
  new zip(archivePath).extractAllTo(targetDir)