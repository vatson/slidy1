archiver = require('./archiver')
fs = require('fs')

exports.upload = (files = []) ->
  files = [].concat files
  for file in files
    do(file) ->
      if archiver.supportsUncompress(file.type)
        archiver.uncompress(file.type, file.path, '/tmp/1')
      fs.unlink(file.path)