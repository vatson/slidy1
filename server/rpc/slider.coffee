exports.actions = (req, res, ss) ->

  # Example of pre-loading sessions into req.session using internal middleware
  req.use('session')

  showPrev:  ->
      ss.publish.all('showPrev')

  showNext:  ->
    ss.publish.all('showNext')