exports.actions = (req, res, ss) ->
  req.use('session')

  getCurrent: () ->
    res(req.session.user)