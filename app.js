// My SocketStream app

var http = require('http')
  , ss = require('socketstream')
  , everyauth = require('everyauth')
;

// Code Formatters
ss.client.formatters.add(require('ss-coffee'));
ss.client.formatters.add(require('ss-jade'));
ss.client.formatters.add(require('ss-stylus'));

// Require own modules
var uploader = require('./server/modules/uploader')

// Define a single-page client
ss.client.define('main', {
  view: 'app.jade',
  css:  ['libs', 'app.styl'],
  code: ['libs', 'app'],
  tmpl: '*'
});

// Serve this client on the root URL
ss.http.route('/', function(req, res){
  res.serveClient('main');
});

ss.http.route('/upload', function(req, res) {
     uploader.upload(req.files.file);
});

// Use server-side compiled Hogan (Mustache) templates. Others engines available
ss.client.templateEngine.use(require('ss-hogan'));

// Minimize and pack assets if you type: SS_ENV=production node app.js
if (ss.env == 'production') ss.client.packAssets();

everyauth.everymodule.logoutPath('/logout');
everyauth.everymodule.handleLogout( function (req, res) {
    req.session.user = null;
    req.session.save();
    this.redirect(res, this.logoutRedirectPath());
});

// Auth via twitter
everyauth.twitter
    .consumerKey('7ZJ2UwtGgB63vUhau9lA4w')
    .consumerSecret('zgJ9hK39dSq10NzOsodFCpBfCAqMcOdbrGpE7N2w6M')
    .findOrCreateUser( function (session, accessToken, accessTokenSecret, twitterUserMetadata) {
        session.user = {
            'name': twitterUserMetadata.screen_name,
            'avatar': twitterUserMetadata.profile_image_url
        }
        session.save();
        return true;
    })
    .handleAuthCallbackError( function (req, res) {
        res.serveClient('main');
    })
    .redirectPath('/');

ss.http.middleware.prepend(ss.http.connect.bodyParser());
ss.http.middleware.append(everyauth.middleware());


// Start web server
var server = http.Server(ss.http.middleware);
server.listen(3000);

// Start SocketStream
ss.start(server);